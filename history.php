<!DOCTYPE html>
<html>

    <head>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
        <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>

        <link rel="stylesheet" href="css/index.css">
    </head>
         
        <body>
            <!--Bagian Navbar-->
            <nav id="navbar" class="navbar navbar-expand-lg navbar-light bg-light">
                <div class="container-fluid">
                    <a class="navbar-brand" href="#">datadiri.lol</a>
                    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                        <li class="nav-item">
                            <a class="nav-link active" aria-current="page" href="index.php">Beranda</a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link active" aria-current="page" href="history.php">History</a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link active" aria-current="page" href="kontak.php">Kontak</a>
                        </li>
                    </ul>
                    </div>
                </div>
            </nav>

            <!--Bagian Konten Colume-->
            <div class="container">
                <div class="row">
                    <div class="col-10 offset-1">
                        <h3 class="page-title">Riwayat Pendidikan</h3>

                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Nama Sekolah</th>
                                    <th scope="col">Alamat</th>
                                    <th scope="col">Jurusan</th>
                                    <th scope="col">IPK</th>
                                </tr>
                            </thead>

                            <tbody>
                                <tr>
                                    <th scope="row">1</th>
                                    <td data-id="name">SDN Ciputat VII</td>
                                    <td>Jl.Aneka Warga Asrama Brimob</td>
                                    <td>-</td>
                                    <td>100</td>
                                </tr>
                                <tr>
                                    <th scope="row">2</th>
                                    <td data-id="name">SMPN 2 Pamulang</td>
                                    <td>Jl.Pamulang Permai No.1</td>
                                    <td>-</td>
                                    <td>100</td>
                                </tr>
                                <tr>
                                    <th scope="row">3</th>
                                    <td data-id="name">SMAN 47 Jakarta</td>
                                    <td>Jl.Tanah Kusir Raya No.1</td>
                                    <td>IPA</td>
                                    <td>100</td>
                                </tr>
                                <tr>
                                    <th scope="row">4</th>
                                    <td data-id="name">Akademi Kimia Analisi Jakarta</td>
                                    <td>Komp.Timah Jl. Margonda No.1</td>
                                    <td>Agroindustri</td>
                                    <td>3.0/4.0</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!--footer-->
            <script src="js/index.js"></script>
        </body>

        
</html>